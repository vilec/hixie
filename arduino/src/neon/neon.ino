#include "Lixie.h" // Include Lixie Library
#include "FastLED.h"

#define DATA_PIN   8 // Pin to drive Lixies
#define NUM_LEDS 80  // How many Lixies you have

static constexpr byte Addresses[10] = {3, 4, 2, 0, 8, 6, 5, 7, 9, 1};
#define LEDS_OFFSET 20
#define NUMBER_OFFSET 10

CRGB leds[NUM_LEDS];

void setup() {
  // put your setup code here, to run once:
  //lix.begin();
  Serial.begin(115200);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  FastLED.setTemperature(Tungsten100W); 
  //FastLED.setBrightness(10);
}

void loop() {

//twoNumbersDemo();
//backgroundDemo();
fadeDemo();

}

void twoNumbersDemo() { 
  for(int number = 1000; number < 9999; number++) { 
    for(int number2 = 0; number2 < 60; number2++) {
       writeNumbers(number, number2);
       delay(100);
    }
 }
}

void backgroundDemo() {
  for (int i = 1234; i < 9999; i++) {
   writeBackground(i);
   delay(1000);
 }
}

void fadeDemo() {
  for (int i = 1234; i < 9999; i++) {
   fade(i, 20);
   delay(1000);
 }
}

void fade(int number, int speed) { 
     CRGB color = CRGB(0, 187, 255);
     for (int fade = 0; fade < 128; fade += 2) {
      FastLED.clear();
      writeNumber(number, color, false, false, CRGB::Black);
      for (int j = 0; j < NUM_LEDS; j++) { 
        leds[j].fadeLightBy(2 * ease8InOutQuad(fade));
      }
      FastLED.show();
      delay(speed);
     }
     for (int fade = 0; fade < 128; fade += 2) {
      FastLED.clear();
      writeNumber(number, color, false, false, CRGB::Black);
      for (int j = 0; j < NUM_LEDS; j++) { 
        leds[j].fadeLightBy(255 - 2 * ease8InOutQuad(fade));
      }
      FastLED.show();
      delay(speed);
     }
}

void writeNumbers(int number, int number2) {
   FastLED.clear();
   writeNumber(number2, CRGB(255, 0, 187), true, false, CRGB::Black);
   writeNumber(number, CRGB(0, 187, 255), false, false, CRGB::Black);
   FastLED.show();
}

void write(int number) {
   FastLED.clear();
   writeNumber(number, CRGB(0, 187, 255), false, false, CRGB::Black);
   FastLED.show();
}

void writeBackground(int number) {
   FastLED.clear();
   // neon
   // writeNumber(number, CRGB(255, 0, 187), false, true, CRGB(0, 4, 8));
   // nixie
   writeNumber(number, CRGB(255,70,15), false, true, CRGB(0,5,13));

   FastLED.show();
}

void writeNumber(int number, CRGB color, boolean dim, boolean bg, CRGB background) { 
 int digits = 0;
 int offset;
 int led1;
 int led2;
 if (bg) {
   fill_solid(leds, NUM_LEDS, background);
 }
 while (number || digits == 0) {
  offset = (LEDS_OFFSET * digits) + Addresses[number % 10];
  leds[offset] = color; 
  leds[offset + NUMBER_OFFSET] = color;
  if (dim) {
    leds[offset].fadeLightBy(128);
    leds[offset + NUMBER_OFFSET].fadeLightBy(128);
  }
  number /= 10;
  digits++;
 }
}

