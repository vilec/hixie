#include <ArduinoJson.h>
#include "Lixie.h" // Include Lixie Library

#define DATA_PIN   8 // Pin to drive Lixies
#define NUM_LIXIES 4  // How many Lixies you have
Lixie lix(DATA_PIN, NUM_LIXIES);

const char START = '/';
const char END = '!';


void setup() {
  Serial.begin(115200);
  Serial.setTimeout(20);
  lix.begin(); // Initialize LEDs
  lix.brightness(255); // 0-255
  lix.white_balance(Tungsten100W); // Default
  Serial.println("Hixi Json Led Controller started");
}
String wholeString;

String readString;
boolean loading = false;
boolean alarm = false;
CRGB alarmColor;
CRGB normalColor;
CRGB initColor;


void loop() {

  while (Serial.available()) {
    delay(1);
    wholeString = Serial.readString();
    Serial.println("JSON message: " + wholeString);
    parseJson(wholeString);   
  } 
  
  if (loading) { 
    lix.sweep(initColor);
  } 

  if (alarm) { 
    Serial.println("ALARMUJE");
    lix.color_fade(alarmColor, 200);
    delay(100);
    lix.color_fade(CRGB(0, 0, 0), 200);
    delay(50);
  }
}

char c;
void parseJson(String input) {
  for(int i=0; i<input.length(); i++){
    c = input[i];
    if (c == START) {
      clearReadString();
    } else if (c == END) {
      parseMessage();
      clearReadString();
    } else {
       readString += c;
    }
  }
}

void clearReadString(){
    readString ="";
}
/*
/{"action":"write-fade","r":255,"g":0,"b":0,"value":6666}!
/{"action":"write-flip","r":255,"g":0,"b":0,"value":5555}!
/{"action":"write","r":255,"g":0,"b":0,"value":1234}!
/{"action":"color","r":255,"g":0,"b":0}!
/{"action":"init","r":0,"g":255,"b":255}!
/{"action":"init-end","r":0,"g":255,"b":255}!
/{"action":"alarm","r":255,"g":0,"b":0}!
/{"action":"alarm-stop"}!
*/
void parseMessage() {

  Serial.println("Read string: " + readString);
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(readString);
  const char* action = root["action"];
  long value          = root["value"];
  int r = root["r"];
  int g = root["g"];
  int b = root["b"];
  
 /* Serial.println("## Message ##");
  Serial.println(action);
  Serial.println(value);
  Serial.println(r);
  Serial.println(g);
  Serial.println(b);
  Serial.println("## END ##");
  */

  if(strcmp(action, "write-fade") == 0) {
    if (loading) {
      stopLoading();
    }
    Serial.println("lix.writeFade()");
    lix.color_fade(CRGB(r,g,b), 200);
    lix.write_fade(value);
  } else if(strcmp(action, "write-flip") == 0) {
    if (loading) {
      stopLoading();
    }
    Serial.println("lix.writeFlip()");
    lix.color(CRGB(r,g,b));
    lix.write_flip(value, 300, 30);
  } else if (strcmp(action, "write") == 0) {
    if (loading) {
      stopLoading();
    }
    Serial.println("lix.write()");
    lix.color(CRGB(r,g,b));
    lix.write(value);
  } else if (strcmp(action, "color") == 0) {
    Serial.println("lix.color()");
    normalColor = CRGB(r,g,b);
    lix.color(normalColor);
    lix.show();
  } else if (strcmp(action, "init") == 0) {
    initColor = CRGB(r, g, b);
    lix.fill_fade_in(initColor, 30);
    loading = true;
  } else if (strcmp(action, "alarm") == 0) {
    Serial.println("alarm on");
    alarmColor = CRGB(r, g, b);
    lix.color_fade(CRGB(0, 0, 0), 200);
    alarm = true;
  } else if (strcmp(action, "alarm-stop") == 0) {
    Serial.println("alarm stop");
    alarm = false;
    lix.color_fade(normalColor, 200);
  } 
}

void stopLoading() { 
  lix.fill_fade_out(CRGB(0, 255, 255), 30);
  loading = false;
}

int stringToInt(String string) {
  char carray[readString.length() + 1];
  readString.toCharArray(carray, sizeof(carray));
  return atoi(carray);
}

