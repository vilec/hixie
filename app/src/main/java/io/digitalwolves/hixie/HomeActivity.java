package io.digitalwolves.hixie;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManager;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import io.digitalwolves.hixie.config.Config;
import io.digitalwolves.hixie.config.Mode;
import io.digitalwolves.hixie.display.HixieController;
import io.digitalwolves.hixie.feature.Features;

public class HomeActivity extends Activity {

    private static String TAG = "HIXIE";

    private HixieController hixie = new HixieController();
    private AudioController audio = new AudioController();

    private Config config;
    private Timer timer;
    private Mode mode = Mode.INITALIZING;
    private Features features = new Features();
    private Gpio button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Init modules
        initDisplay();
        initAudio();
        initFeatures();
        initFirebase();
        initButton();

        // Start main loop
        runUpdater();

        // Intro sound
        audio.playIntro();

        Log.d(TAG, "Hixie App Started");
    }

    private void initAudio() {
        audio.init(this);
    }

    public void changeMode(Mode newMode) {
        if (newMode != mode) {
            Log.d(TAG, "Mode changed from " + mode + " to " + newMode);
            mode = newMode;
            features.updateCurrentDisplay(mode);
        }
    }

    private void runUpdater() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (config != null) {
                    features.update(config);
                    features.updateCurrentDisplay(mode);
                }
            }
        }, 500, 1000);
    }

    private void initFirebase() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                config = dataSnapshot.getValue(Config.class);
                Log.d(TAG, "Update: " + config);
                features.update(config);
                changeMode(Mode.valueOf(config.mode.toUpperCase()));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initButton() {
        try {
            button = PeripheralManager.getInstance().openGpio("BCM21");
            button.setDirection(Gpio.DIRECTION_IN);
            button.setEdgeTriggerType(Gpio.EDGE_FALLING);
            button.registerGpioCallback(gpio -> {
                Log.i(TAG, "GPIO changed, button pressed");
                features.notifiyButtonClicked(mode);
                return true;
            });
        } catch (IOException e) {
            Log.e(TAG, "Button doesn't work", e);
        }
    }

    private void initDisplay() {
        hixie.init();
        hixie.startLoading();
    }

    private void initFeatures() {
        features.init(hixie, audio, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        audio.stop();
        hixie.onDestroy();
        timer.cancel();
        try {
            button.close();
        } catch (IOException e) {
            Log.e(TAG, "Couldn't close button");
        }
        Log.d(TAG, "Hixie App Closed");
    }

    public Mode getCurrentMode() {
        return mode;
    }
}
