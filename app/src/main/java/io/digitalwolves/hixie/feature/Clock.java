package io.digitalwolves.hixie.feature;


import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import io.digitalwolves.hixie.AudioController;
import io.digitalwolves.hixie.HomeActivity;
import io.digitalwolves.hixie.config.Config;
import io.digitalwolves.hixie.config.Mode;
import io.digitalwolves.hixie.display.HixieController;

public class Clock extends Feature {

    private static final String TAG = "Clock";

    private String time;
    private Config config;

    @Override
    public void init(HixieController hixie, AudioController audio, HomeActivity homeActivity) {
        super.init(hixie, audio, homeActivity);
        update(null);
    }

    @Override
    public void update(Config config) {
        this.config = config;
        time = currentTime();
    }

    @Override
    public void updateDisplay() {
        hixie.writeFade(time, config.r, config.g, config.b);
        Log.d(TAG, "Current Time: " + time);
    }

    private String currentTime() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HHmm");
        date.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));
        return date.format(currentLocalTime);
    }

    @Override
    public Mode getMode() {
        return Mode.CLOCK;
    }

    @Override
    public void onButtonClicked() {
        homeActivity.changeMode(Mode.ALARM);
    }
}
