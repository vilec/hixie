package io.digitalwolves.hixie.feature;


import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.digitalwolves.hixie.AudioController;
import io.digitalwolves.hixie.HomeActivity;
import io.digitalwolves.hixie.config.Config;
import io.digitalwolves.hixie.config.Mode;
import io.digitalwolves.hixie.display.HixieController;

public class Features {

    private static final String TAG = "Features";

    List<Feature> features = new ArrayList<>();

    public void init(HixieController hixie, AudioController audio, HomeActivity homeActivity) {
        features.add(new Clock());
        features.add(new Alarm());
        features.add(new Instagram());
        features.add(new Counter());
        features.forEach(f -> f.init(hixie, audio, homeActivity));
    }

    public void update(Config config) {
        features.forEach(f -> f.update(config));
    }

    public void updateCurrentDisplay(Mode mode) {
        Feature currentFeature = getByMode(mode);
        if (currentFeature != null) {
            currentFeature.updateDisplay();
        } else {
            Log.e(TAG, "There is no mode called: " + mode);
        }
    }

    public Feature getByMode(Mode mode) {
        return features.stream().filter(f -> f.getMode() == mode).findFirst().orElse(null);
    }

    public void notifiyButtonClicked(Mode mode) {
        Feature currentFeature = getByMode(mode);
        if (currentFeature != null) {
            currentFeature.onButtonClicked();
        }
    }
}
