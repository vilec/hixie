package io.digitalwolves.hixie.feature;

import io.digitalwolves.hixie.config.Config;
import io.digitalwolves.hixie.config.Mode;

public class Counter extends Feature {

    private Config config;
    private int seconds;
    private int oldSeconds;
    private int counter;

    private boolean running = false;

    @Override
    public void update(Config config) {
        this.config = config;
        oldSeconds = seconds;
        seconds = config.seconds;
        if (oldSeconds != 0 && oldSeconds != seconds) {
            startCounter();
        }
    }

    private void startCounter() {
        counter = seconds;
        running = true;
    }

    @Override
    public void updateDisplay() {
        if (running) {
            hixie.writeFade(String.valueOf(counter), config.r, config.g, config.b);
            if (counter == 0) {
                running = false;
            }
            counter--;
        } else {
            hixie.writeFade(String.valueOf(0), config.r, config.g, config.b);
        }
    }

    @Override
    public void onButtonClicked() {
        homeActivity.changeMode(Mode.CLOCK);
    }

    @Override
    public Mode getMode() {
        return Mode.COUNTER;
    }
}
