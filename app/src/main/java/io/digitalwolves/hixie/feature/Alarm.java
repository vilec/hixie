package io.digitalwolves.hixie.feature;


import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import io.digitalwolves.hixie.AudioController;
import io.digitalwolves.hixie.HomeActivity;
import io.digitalwolves.hixie.config.Config;
import io.digitalwolves.hixie.config.Mode;
import io.digitalwolves.hixie.display.HixieController;

public class Alarm extends Feature {

    private static final String TAG = "Alarm";
    private String alarm;
    private String currentTime;
    private boolean active = false;
    private boolean ringing = false;

    private Mode oldMode = Mode.CLOCK;

    @Override
    public void init(HixieController hixie, AudioController audio, HomeActivity homeActivity) {
        super.init(hixie, audio, homeActivity);
        currentTime = currentTime();
    }

    public void update(Config config) {
        currentTime = currentTime();
        setNewAlarm(config);
        checkIfAlarmFires();
    }

    private void checkIfAlarmFires() {
        if (active && !ringing && currentTime.equals(alarm)) {
            Log.d(TAG, "RING RANG RONG");
            startAlarm();
        }
    }

    private void setNewAlarm(Config config) {
        String newAlarm = config.alarm_time;
        if (newAlarm != null && !newAlarm.equals(alarm)) {
            alarm = newAlarm;
            active = true;
            Log.d(TAG, "Alarm set to: " + alarm);
        }
    }

    @Override
    public void onButtonClicked() {
        if (ringing) {
            audio.stop();
            hixie.stopAlarm();
            homeActivity.changeMode(oldMode);
            active = false;
            ringing = false;
        } else {
            homeActivity.changeMode(Mode.INSTAGRAM);
        }

    }

    public void startAlarm() {
        ringing = true;
        oldMode = homeActivity.getCurrentMode();
        homeActivity.changeMode(Mode.ALARM);
        hixie.startAlarm();
        audio.playAlarm();
    }

    public void updateDisplay() {
        if (ringing) {
            hixie.write(currentTime, 255, 0, 0);
            return;
        }
        hixie.writeFade(alarm, 255, 0, 0);
    }

    private String currentTime() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+1:00"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HHmm");
        date.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));
        return date.format(currentLocalTime);
    }

    @Override
    public Mode getMode() {
        return Mode.ALARM;
    }
}
