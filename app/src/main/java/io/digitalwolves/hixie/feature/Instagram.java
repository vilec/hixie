package io.digitalwolves.hixie.feature;


import io.digitalwolves.hixie.config.Config;
import io.digitalwolves.hixie.config.Mode;

public class Instagram extends Feature {

    private int instagramFollowers;
    private Config config;

    @Override
    public void update(Config config) {
        this.config = config;
        instagramFollowers = config.instagram_followers;
    }

    @Override
    public void updateDisplay() {
        hixie.writeFlip(String.valueOf(instagramFollowers), config.r, config.g, config.b);
    }

    @Override
    public Mode getMode() {
        return Mode.INSTAGRAM;
    }

    @Override
    public void onButtonClicked() {
        homeActivity.changeMode(Mode.COUNTER);
    }
}
