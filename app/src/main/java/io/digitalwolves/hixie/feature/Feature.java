package io.digitalwolves.hixie.feature;

import io.digitalwolves.hixie.AudioController;
import io.digitalwolves.hixie.HomeActivity;
import io.digitalwolves.hixie.config.Config;
import io.digitalwolves.hixie.config.Mode;
import io.digitalwolves.hixie.display.HixieController;

public abstract class Feature {

    protected HixieController hixie;
    protected AudioController audio;
    protected HomeActivity homeActivity;

    public void init(HixieController hixie, AudioController audio, HomeActivity homeActivity) {
        this.hixie = hixie;
        this.audio = audio;
        this.homeActivity = homeActivity;
    }

    abstract public void update(Config config);

    abstract public void updateDisplay();

    abstract public Mode getMode();

    abstract  public void onButtonClicked();

}
