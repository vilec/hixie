package io.digitalwolves.hixie.config;

public class Config {
    public int r;
    public int g;
    public int b;
    public String mode;
    public int instagram_followers;
    public String alarm_time;
    public int seconds;

    @Override
    public String toString() {
        return "Config{" +
                "r = " + r +
                ", g = " + g +
                ", b = " + b +
                ", mode = '" + mode + '\'' +
                ", instagram_followers = " + instagram_followers +
                ", alarm_time = '" + alarm_time + '\'' +
                ", seconds = '" + seconds + '\'' +
                '}';
    }
}
