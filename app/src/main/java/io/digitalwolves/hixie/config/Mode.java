package io.digitalwolves.hixie.config;

public enum Mode {
    INITALIZING, CLOCK, ALARM, INSTAGRAM, COUNTER;
}