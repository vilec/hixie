package io.digitalwolves.hixie.display;


public class HixieMessage {

    public String action;
    public int value;
    public int r;
    public int g;
    public int b;
}
