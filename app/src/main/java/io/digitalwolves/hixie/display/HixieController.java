package io.digitalwolves.hixie.display;


import android.util.Log;

import com.google.android.things.pio.PeripheralManager;
import com.google.android.things.pio.UartDevice;
import com.google.gson.Gson;

import java.io.IOException;
import java.nio.charset.Charset;

public  class HixieController {

    private static final String TAG = "HixieController";

    private UartDevice arduino;
    private Gson gson;

    public HixieController() {
        gson = new Gson();
    }

    public void init() {
        PeripheralManager peripheralManager = PeripheralManager.getInstance();
        try {
            arduino = peripheralManager.openUartDevice("UART0");
            arduino.setBaudrate(115200);
            arduino.setDataSize(8);
            arduino.setStopBits(1);
            arduino.setParity(0);
            arduino.setHardwareFlowControl(UartDevice.HW_FLOW_CONTROL_AUTO_RTSCTS);
        } catch (IOException e) {
            Log.e(TAG, "Couldn't connect to Arduino");
        }
    }

    public void writeFade(String value, int r, int g, int b) {
        HixieMessage message = new HixieMessage();
        message.action = "write-fade";
        message.value = Integer.valueOf(value);
        message.r = r;
        message.g = g;
        message.b = b;
        sendMessage(message);
    }

    public void write(String value, int r, int g, int b) {
        HixieMessage message = new HixieMessage();
        message.action = "write";
        message.value = Integer.valueOf(value);
        message.r = r;
        message.g = g;
        message.b = b;
        sendMessage(message);
    }

    public void writeFlip(String value, int r, int g, int b) {
        HixieMessage message = new HixieMessage();
        message.action = "write-flip";
        message.value = Integer.valueOf(value);
        message.r = r;
        message.g = g;
        message.b = b;
        sendMessage(message);
    }

    public void color(int r, int g, int b) {
        HixieMessage message = new HixieMessage();
        message.action = "color";
        message.r = r;
        message.g = g;
        message.b = b;
        sendMessage(message);
    }

     synchronized private void sendMessage(HixieMessage message) {
        try {
            String json = gson.toJson(message);
            byte[] bytes = ("/" + json + "!").getBytes(Charset.forName("UTF-8"));
            arduino.write(bytes, bytes.length);
        } catch (IOException e) {
            Log.e(TAG, "Couldn't send message to Arduino");
        }
    }

    public void onDestroy() {
        try {
            arduino.close();
        } catch (IOException e) {
            Log.e(TAG, "Couldn't close Arduino");
        }
    }

    public void startLoading() {
        HixieMessage message = new HixieMessage();
        message.action = "init";
        message.r = 0;
        message.g = 255;
        message.b = 255;
        sendMessage(message);
        Log.d(TAG, "Start loading animation");
    }

    public void startAlarm() {
        HixieMessage message = new HixieMessage();
        message.action = "alarm";
        message.r = 255;
        message.g = 0;
        message.b = 0;
        sendMessage(message);
        Log.d(TAG, "Start alarm animation");
    }

    public void stopAlarm() {
        HixieMessage message = new HixieMessage();
        message.action = "alarm-stop";
        sendMessage(message);
        Log.d(TAG, "Stop alarm animation");
    }
}
