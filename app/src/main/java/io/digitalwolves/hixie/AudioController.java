package io.digitalwolves.hixie;


import android.media.MediaPlayer;

public class AudioController {

    private static final String TAG = "AudioController";

    private MediaPlayer mediaPlayer;
    private HomeActivity homeActivity;

    private boolean isPlaying = false;

    public void init(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    public void playAlarm() {
        if (isPlaying) {
            return;
        }
        isPlaying = true;
        mediaPlayer = MediaPlayer.create(homeActivity.getApplicationContext(), R.raw.test);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    public void playIntro() {
        mediaPlayer = MediaPlayer.create(homeActivity.getApplicationContext(), R.raw.started);
        mediaPlayer.start();
    }

    public void stop() {
        mediaPlayer.stop();
        isPlaying = false;
    }

}
